const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const GroupSchema = new Schema({
  group_name: {
    type: String,
    required: true,
  },
  group_img: {
    type: String,
    default:'https://coding-x-service-file.s3.ap-northeast-2.amazonaws.com/default_img/default_profile.svg',
  },
  group_manager_oid: [String], //그룹 관리자, 그룹유저말고 일반유저 oid입니다.
  group_type: {
    type: Number,
    required: true,
  },
  //0대학교 1고등학교 2중학교 3초등학교 4학원 5부트캠프 6일반인(개인) 7스터디팀 8자가학습 9기타
  group_desc: {
    type: String,
    default: '0',
  },
  group_url: {
    type: String,
    unique: true,
  },

  group_open_date: {
    type: Date,
    default: Date.now,
  },
  group_exposure: {
    type: Boolean,
    default: true,
  },
  group_visible: {
    type: Boolean,
    default: true,
  },
  //등록된 리더
  group_leader: [String],

  //오늘 방문 수
  today_visit: {
    type: Number,
    default: 0,
  },
  group_learner: [String], //그룹유저말고 일반유저 oid입니다.
  group_master_email: {
    type: String,
    default: 'contact@codingrobotlab.com',
  },
  group_master_name: {
    type: String,
    default: '서비스관리자',
  },
  group_master_user_oid: {
    type: String,
  },
  group_master_user_type: {
    type: String,
  },
  // group_class_list : [GroupClassSchema],  //not yet
});

const GroupData = mongoose.model('groupdata', GroupSchema);
module.exports = GroupData;
