const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const GroupClassNotiSchema = new Schema({
  group_class_noti_title: {
    type: String,
    required: true,
  },
  group_class_noti_content: {
    type: String,
  },
  group_class_noti_date: {
    type: Date,
    default: Date.now,
    required: true,
  },
  group_class_noti_view: {
    type: Number,
    default: 0,
    required: true,
  },
  group_class_noti_file: {
    type: String,
  },
  group_class_noti_visible: {
    type: Boolean,
    default: true,
  },
  group_class_noti_writer: {
    type: String,
  },
});

const GroupClassSchema = new Schema({
  group_oid: {
    type: String,
    default: "600d259544b48a5fa570d69e"
  }, //어디에 속한 클래스인지
  group_class_name: {
    type: String,
    required: true,
  },
  group_class_number: {
    type: String,
  },
  group_class_img: {
    type: String,
    default:
      'https://coding-x-service-file.s3.ap-northeast-2.amazonaws.com/default_img/freecomputer.jpeg',
  },
  group_class_syllabus: String,
  group_class_desc: String,
  group_class_leader: [String],
  group_class_manager_oid: [String],
  group_class_create_date: {
    type: Date,
    default: Date.now,
  },
  group_class_start_date: Date,
  group_class_end_date: Date,
  group_class_open_range: {
    type: Number,
    default: 2,
  },
  //0이 전체공개, 1이 그룹멤버 공개, 2가 클래스멤버 공개, 3이 비공개
  group_class_noti: [GroupClassNotiSchema],
  group_class_learner: [String],
  group_class_creator_name: {
    type: String,
    required: true,
    default: '-',
  },
  group_class_creator_dept: {
    type: String,
  },
  group_class_creator_profile: {
    type: String,
    default:
      'https://coding-x-service-file.s3.ap-northeast-2.amazonaws.com/default_img/default_profile.svg',
  },
  group_class_creator_email:String,
  group_class_language: {
    type: [String],
    default: ["C", "C++", "Java", "Python"]
  },
  group_class_level: {
    type: Number,
    default: 0,
  },
});

const GroupClassData = mongoose.model('groupclassdata', GroupClassSchema);
module.exports = GroupClassData;
