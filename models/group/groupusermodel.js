const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate-v2');

const GroupUserSchema = new Schema({
  group_oid: {
    type: String,
    default: '600d259544b48a5fa570d69e',
  }, //그룹 oid
  group_class_oid: {
    type: [String],
    //required:true,
  }, //소속된 그룹 클래스 oid
  group_user_name: {
    type: String,
    required: true,
  }, //유저의 이름
  group_user_nickname: String, // 아주대 삭제.
  group_user_email: String,
  group_invitation_path: {
    type: Number,
    default: 0
  },
  // 0관리자 지정 1링크초대 2메일초대
  user_oid: {
    type: String,
    required: true,
  }, //UserDB의 oid 참조
  user_status: {
    type: Number,
    required: true,
    default: 1,
  }, //해당 그룹에서 user의 상태: 요청(0),완료(1),탈퇴(2),강퇴(3)
  user_status_date: {
    type: Date,
    default: Date.now,
  }, //user의 상태가 변한 마지막 날: (초대된 날, 탈퇴한 날)
  group_user_profile_img: {
    type: String,
    default:
      'https://coding-x-service-file.s3.ap-northeast-2.amazonaws.com/default_img/default_profile.svg',
  },
  group_user_stdno: Number, //대학생일 경우 학번
  group_user_dept: String, //대학생일 경우 학과
  group_user_type: { type: Number, required: true }, //0그룹장 1매니저 2리더 3멤버 4조교
  group_user_leader_type: String,
});

GroupUserSchema.plugin(mongoosePaginate);

const GroupUserData = mongoose.model('groupuserdata', GroupUserSchema);
GroupUserData.paginate().then({});
module.exports = GroupUserData;
