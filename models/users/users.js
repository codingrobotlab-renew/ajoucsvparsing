const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const usersSchema = new Schema({
    user_id: String,
    user_name: String,
    user_nickname: String,
    user_password: String,
    user_profile_img: {
        type: String,
        default: 'https://coding-x-service-file.s3.ap-northeast-2.amazonaws.com/default_img/default_profile.svg'
    },
    is_authenticated: {
        type: Boolean,
        default: true
    },
    user_job: {
        type:String,
        default:null
    },
    major_a: {
        type:String,
        default:null
    },
    major_b: {
        type:String,
        default:null
    },
    major_c: {
        type:String,
        default:null
    },
    user_url: {
        type:String,
        default:null
    },
    introduce_title: {
        type:String,
        default:null
    },
    introduce_detail: {
        type:String,
        default:null
    },
    date: {
        type: Date,
        default: Date.now
    },
    is_seller: {
        type:Boolean,
        default:false
    },
    is_seller_visible:{
        type:Boolean,
        default:false
    }
});

const UserData = mongoose.model('userData', usersSchema);
module.exports = UserData;