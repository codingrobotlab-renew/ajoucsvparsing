const express = require('express');
const csv = require('csv-parser');
const fs = require('fs');
const UserSchema = require('../models/users/users');
const GroupUserData = require('../models/group/groupusermodel');
const GroupClassData = require('../models/group/groupclassmodel');

const router = express.Router();

router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/groupclass', (req, res)=>{
  const classlist = [];
  fs.createReadStream('classname.csv')
  .pipe(csv())
  .on('data', (data)=>classlist.push(data))
  .on('end', ()=>{
    console.log(classlist);
    var classmakelist = async() =>{
      var classcheck
      classcheck = await classlist.map(async classes =>{
        const makeClass = await GroupClassData.findOne({
          group_class_name: classes.KNA,
        }, (err, result)=>{
          if(!result){
            console.log('hello');
            UserSchema.findOne({user_name:classes.PROF}, (err, result)=>{
              if(result)
              {
                var ajouClass = new GroupClassData({
                  group_class_name: classes.KNA,
                  group_class_number: classes.HAKNO,
                  group_class_desc : classes.KNA,
                  group_class_leader : result._id,
                  group_class_creator_name: classes.PROF,
                  group_class_creator_dept: classes.PSOSOKNA,
                  group_class_creator_email: result.user_id
                })

                ajouClass.save().then(()=>{
                  console.log('수업저장').catch(err=>{
                    console.log(err);
                  })
                })
              }
            });
          }
          else{
            console.log('중복')
          }
        }).catch((err)=>{
          console.log(err);
        });
      })
    }
    classmakelist();
  })
  return res.send('교실만들기')
});

router.get('/groupuser', (req, res)=>{

});

router.get('/userid_student', (req,res)=>{
  const results = [];
  fs.createReadStream('list.csv')
  .pipe(csv())
  .on('data', (data) => results.push(data))
  .on('end', () => {
    var user = async() =>{
      var userCheck;

      userCheck = await results.map(async emails =>{
        const check = await UserSchema.findOne({
          user_id: emails.EMAIL
        }, (err, result) => {
          if(!result){
                 var user = new UserSchema({
                   user_id: emails.EMAIL,
                   user_name: emails.NAME
                 });

                 user.save().then((userresult) => {
                   console.log("저장")
                   var groupstudent = new GroupUserData({
                     group_user_name: userresult.user_name,
                     group_user_nickname: userresult.user_name,
                     group_user_email: userresult.user_id,
                     user_oid:userresult._id,
                     group_user_type:3,
                     group_user_leader_type:'학생',
                     group_user_stdno:emails.HAKBUN,
                     group_user_dept:emails.MAJOR1NM,
                   });

                   groupstudent.save().then(()=>{
                     console.log('그룹학생저장');
                   }).catch(err=>{
                     console.log(err);
                   })
                 }).catch(err => {
                   console.log(err);
                 });
          }
          else{
            console.log("중복");
          }
        }).catch(err => {
          console.log(err);
        })
      })
    }
    user();
  });
  return res.send("저장완료")  
})

//교수 등록.
router.get('/userid_prof', (req,res)=>{
  const users_pf = [];
  fs.createReadStream('VW_USEAT_PROF_NAMEONLY.csv')
  .pipe(csv())
  .on('data', (data) => users_pf.push(data))
  .on('end', () => {
    var user = async() =>{
      var userCheck;
      console.log(users_pf);

      userCheck = await users_pf.map(async csvdata =>{
        UserSchema.findOne({user_id:csvdata.EMAIL},
          (err, result)=>{
            if(!result)
            {
                 var user = new UserSchema({
                   user_id: csvdata.EMAIL,
                   user_name: csvdata.PROF
                 });
                 user.save().then((saveresult) => {
                   console.log("저장");
                   console.log(saveresult._id);
                   var groupuser = new GroupUserData({
                        group_user_name: csvdata.PROF,
                        group_user_nickname: csvdata.PROF,
                        group_user_email: csvdata.EMAIL,
                        user_oid: saveresult._id,
                        group_user_type: 2,
                        group_user_leader_type:'교수'
                      })
                      groupuser.save().then(()=>{
                        console.log('그룹유저저장');
                      }).catch(err =>{
                        console.log(err);
                      })
                 }).catch(err => {
                   console.log(err);
                 });
            }
  
            else{
              console.log("찾았다. 혹은 중복");
            }
        });
      })
    }
    user();
  })
  res.send("유저교수저장");
})

router.get('/csvread',(req, res)=>{
  let readcsv =[]
  fs.createReadStream('VW_USEAT_LEC_STU_Time.csv')
  .pipe(csv())
  .on('data', (data) => readcsv.push(data))
  .on('end', () => {
    console.log(readcsv);
  })
});

router.get('/registerclass', (req, res)=>{
  let readcsv =[]
  fs.createReadStream('VW_USEAT_LEC_STU_Time.csv')
  .pipe(csv())
  .on('data', (data) => readcsv.push(data))
  .on('end', () => {
    // var registerclass = async() =>{
    //   let crossAdd;
    //   corssAdd = await readcsv.map(async crossdata =>{
    //     console.log("csv data: \n"+crossdata)

    //     GroupUserData.findOne({group_user_stdno:crossdata.HAKBUN})
    //     .then((student)=>{
    //       GroupClassData.findOne({group_class_number:crossdata.HAKNO})
    //       .then((groupclass)=>{
    //         console.log("studentdata: \n"+student);
    //         console.log("classdata: \n"+groupclass);
    //       });
    //     })
    //   });
    // }
    GroupUserData.findOne({group_user_stdno:readcsv[0].HAKBUN})
    .then((student)=>{
      GroupClassData.findOne({group_class_number:readcsv[0].HAKNO})
      .then((groupclass)=>{
        console.log("studentdata: \n"+ student);
        console.log("groupdata: \n"+ groupclass);
        console.log(student.group_class_oid);
        console.log(groupclass.gropu_class_learner);
        // student.group_class_oid.push(groupclass._id);
        // groupclass.group_class_learner.push(student.user_oid);
        // student.save();
        // groupclass.save();
      })
    })
    // registerclass();
  });
  res.send("그룹수업등록");
});

module.exports = router;
